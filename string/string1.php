<?php
echo"<br> HEREDOC STRING<br>";
$str = <<<EOD
Example of string
spanning multiple lines
using heredoc syntax.
EOD;
echo'<pre>';
print_r($str);

//addcslashes
echo"\n<br>ADDCSLASHES<br>";
echo addcslashes('foo[ ]', 'A..z');
$str = "Welcome to my humble Homepage!";
echo "<br>".$str."<br>";
echo addcslashes($str,'m')."<br>";
echo addcslashes($str,'H')."<br>";

//addslashes
$str = "Who's Peter Griffin?";
echo "\nADDSLASHES<br>".$str . " This is not safe in a database query.<br>";
echo addslashes($str) . " This is safe in a database query.";


//chop
echo"\n<br> CHOP <br>";
$str1 = "Hello World!";
echo $str1 . "<br>";
echo chop($str1,"World!");

//chunk_split
echo "\n<br> CHUNK_SPLIT<br>";
echo chunk_split($str1,1,".");

//count_chars
echo"\n<br> COUNT_CHARS<br>";
echo count_chars($str1,3);


//crc32
echo"\n<br> CRC32<br>";
$str2 = crc32("Hello world.");
echo 'Without %u: '.$str2."<br>";
echo 'With %u: ';
printf("%u",$str2);

// crypt
echo"\n<br> CRYPT<br>";
// 2 character salt
if (CRYPT_STD_DES == 1)
{
echo "Standard DES: ".crypt('something','st')."\n<br>"; 
}
else
{
echo "Standard DES not supported.\n<br>";
}

// 4 character salt
if (CRYPT_EXT_DES == 1)
{
echo "Extended DES: ".crypt('something','_S4..some')."\n<br>";
}
else
{
echo "Extended DES not supported.\n<br>";
}
//htmlentities
echo"\n <br>HTML ENTITIES<br>";
$str3 = "A 'quote' is <b>bold</b>";
echo htmlentities($str3);
echo htmlentities($str3, ENT_QUOTES);

//htmlspecialchars
echo"\n <br> HTMLSPECIALCHARS<br>";
$str4 = 'I love "PHP".';
echo htmlspecialchars($str4, ENT_QUOTES);

?>

