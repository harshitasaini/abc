<?php
//implode()
$arr = array('Hello','World!','Beautiful','Day!');
echo"\n<br> IMPLODE<br>";
echo implode(" ",$arr)."<br>";
echo implode("+",$arr)."<br>";
echo implode("-",$arr)."<br>"; 
echo implode("X",$arr)."<br>";

//join
echo "\n<br> JOIN <br>";
echo "above example<br>";
 
//MD5
echo "\n<br>MD5<br>"; 
$str = "Hello"; 
echo "<The string: ".$str."<br>"; 
echo "TRUE - Raw 16 character binary format: ".md5($str, TRUE)."<br>"; 
echo "FALSE - 32 character hex number: ".md5($str)."<br>"; 

//parse_str
echo"\n<br>PARSE_STR<br>";
parse_str("name=Peter&age=43",$myArray);
print_r($myArray);
echo"<br>";

//str_replace
echo"<br> STR_REPLACE<br>";
$arr1 = array("blue","red","green","yellow");
print_r(str_replace("red","pink",$arr1,$i));
echo "Replacements: $i<br>";

//strstr
echo "<br> STRSTR<br>";
echo strstr("Hello world!","world");  // find the 1st occurence of world in hello world and rturn rest of the string
echo '<br>';
echo strstr("Hello world!",111);     //Search a string for the ASCII value of "o" and return the rest of the string
echo '<br>';

//substr
echo"<br> SUBSTR<br>";
echo substr("Hello world",10)."<br>";
echo substr("Hello world",1)."<br>";
echo substr("Hello world",3)."<br>";
echo substr("Hello world",-10)."<br>";
echo substr("Hello world",-8)."<br>";
echo substr("Hello world",-4)."<br>";
?>
