<?php
$input = array("a", "b", "c", "d", "e");
echo '<pre>';
$output = array_slice($input, 2); print_r($output);     // returns "c", "d", and "e"
$output = array_slice($input, -2, 2); print_r($output); // returns "d"
$output = array_slice($input, 0, 3);   // returns "a", "b", and "c"

// note the differences in the array keys
print_r(array_slice($input, 2, -1));
print_r(array_slice($input, 2, -1, true));
?>
