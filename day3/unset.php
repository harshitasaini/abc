<?php
$arr = array(1, 2, 3, 4);
foreach ($arr as $value) {
    $value1[] = $value * 2;
}
// $arr is now array(2, 4, 6, 8)

unset($value); 
echo '<pre>';
unset($value1[2]);
print_r($value1);
// break the reference with the last element

?>
