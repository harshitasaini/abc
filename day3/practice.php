<?php
$fruits = array("d" => "lemon", "a" => "orange", "b" => "banana", "c" => "apple");

function test_alter(&$item1, $key, $prefix)
{
   //echo 'test'; 
   //print($item1); die;
    $item1 = "$prefix: $item1";
   echo $item1;
}

function test_print($item2, $key)
{
    echo "$key. $item2<br />\n";
}

echo "Before ...:\n";
array_walk($fruits, 'test_print');


echo "... and after:\n";
array_walk($fruits, 'test_alter', 'fruit');

//array_walk($fruits, 'test_print');
?>
